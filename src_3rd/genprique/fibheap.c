/*

genprique - microlib for priority queues - fibonacci heap

Copyright (c) 2020 Tibor 'Igor2' Palinkas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/genprique/trunk
Contact the author: http://igor2.repo.hu/contact.html

*/

#include <stddef.h>
#include <string.h>
#include <assert.h>
#include "fibheap.h"

GENPRIQUE_API int fbh_init(fbh_t *fbh, unsigned long offs)
{
	fbh->offs = offs;
	fbh->min = NULL;
	fbh->num_nodes = 0;
	return 0;
}

/* remove src from its list and return the node that was right to it */
static fbhn_t *clist_pop(fbhn_t *src)
{
	fbhn_t *ret = src->right;
	if (src->right != src) {
		src->left->right = src->right;
		src->right->left = src->left;
	}
	else
		ret = NULL;
	src->left = src->right = src;
	src->parent = NULL;
	return ret;
}

/* insert src left of dst */
static void clist_insert_left(fbhn_t **dst, fbhn_t *src)
{
	assert(src->parent == NULL);
	if (*dst != NULL) {
		src->left = (*dst)->left;
		src->right = (*dst);
		(*dst)->left->right = src;
		(*dst)->left = src;
	}
	else {
		*dst = src;
		(*dst)->left = (*dst)->right = *dst;
	}
}

/* concat src_min to the list in fbh; src_min needs to be the minimum in the source list */
static void fbh_list_concat_(fbh_t *fbh, fbhn_t *src_min)
{
	fbhn_t *n, *next;

	for(n = src_min; n != NULL; n = next) {
		next = clist_pop(n);
		clist_insert_left(&fbh->min, n);
		fbh->num_nodes++;
	}
	if ((fbh->min == NULL) || (src_min->pri <= fbh->min->pri))
		fbh->min = src_min;
}

GENPRIQUE_API int fbh_insert(fbh_t *fbh, void *data, GENPRIQUE_PRI_TYPE pri)
{
	fbhn_t *nd = fbh_o2n(fbh, data);

	nd->parent = NULL;
	nd->deg = 0;
	nd->child = NULL;
	nd->left = nd->right = nd;
	nd->mark = 0;
	nd->pri = pri;
	fbh_list_concat_(fbh, nd);

	return 0;
}

GENPRIQUE_API int fbh_concat_heap(fbh_t *dst, fbh_t *src)
{
	if (src->offs != dst->offs)
		return -1;
	if (src->min == NULL)
		return 0;
	fbh_list_concat_(dst, src->min);
	src->min = NULL;
	src->num_nodes = 0;
	return 0;
}


static void fbh_relink(fbh_t *fbh, fbhn_t *y, fbhn_t *x)
{
	fbhn_t *newn, *ypar = y->parent;

	newn = clist_pop(y);
	if (ypar != NULL)
		ypar->child = newn;
	clist_insert_left(&x->child, y);
	y->parent = x;
	x->deg++;
	y->mark = 0;
}

static void fbh_consolidate(fbh_t *fbh)
{
	fbhn_t *tmp, *nextw, *lastw, *x, *y, *w, *A[32];
	int d, i;

	if (fbh->min == NULL)
		return;

	memset(A, 0, sizeof(A));

	w = fbh->min;
	lastw = w->left; /* need to remember last item to check as new items will be inserted between last and fhb->min and they shouldn't be revisited */
	do {
		x = w;
		d = x->deg;
		if (w != lastw)
			nextw = w->right;
		else
			nextw = NULL;
		assert(d < 32);
		while(A[d] != NULL) {
			y = A[d];
			if (x->pri > y->pri) {
				tmp = x;
				x = y;
				y = tmp;
			}
			fbh_relink(fbh, y, x);
			A[d] = NULL;
			d++;
		}
		A[d] = x;
		w = nextw;
	} while(w != NULL);

	/* insert degree nodes */
	fbh->min = NULL;
	for(i = 0; i < 32; i++) {
		if (A[i] != NULL) {
			A[i]->parent = NULL;
			clist_insert_left(&fbh->min, A[i]);
			if (A[i]->pri < fbh->min->pri)
				fbh->min = A[i];
		}
	}
}

GENPRIQUE_API void *fbh_pop_min(fbh_t *fbh)
{
	fbhn_t *z, *zr, *x, *next;

	if ((fbh->min == NULL) || (fbh->num_nodes == 0))
		return NULL;

	z = fbh->min;

	x = z->child;
	if (x != NULL) {
		do {
			next = x->right;
			x->parent = NULL;
			clist_insert_left(&fbh->min, x);
			x = next;
		} while(x != z->child);
	}

	zr = z->right;
	clist_pop(z);
	if (zr == NULL)
		fbh->min = NULL;
	else {
		fbh->min = zr;
		fbh_consolidate(fbh);
	}
	fbh->num_nodes--;
	if (fbh->num_nodes == 0)
		fbh->min = NULL;
	return fbh_n2o(fbh, z);
}
