/*
    libminuid - minimalistic globally unique IDs
    Copyright (C) 2022  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Project page:  http://repo.hu/projects/libminuid
    Source code:   svn://repo.hu/libminuid/trunk
    Author:        emailto: libminuid [at] igor2.repo.hu
*/

#include <stdio.h>
#include "libminuid.h"

static int help(const char *prg)
{
	printf("minuid - command line utility for globally unique IDs\n");
	printf("Usage: %s cmd\n", prg);
	printf("cmd is one of:\n");
	printf("  help        this help screen\n");
	printf("  gen         generate UIDs on stdout\n");
	printf("\n");
	return 0;
}

static int gen(void)
{
	minuid_session_t sess;

	if (minuid_init(&sess) != 0) {
		fprintf(stderr, "minuid_init() failed\n");
		return 1;
	}

	for(;;) {
		minuid_bin_t bin;
		minuid_str_t txt;

		if (minuid_gen(&sess, bin) != 0) {
			fprintf(stderr, "minuid_gen() failed\n");
			return 1;
		}
		if (minuid_bin2str(txt, bin) != 0) {
			fprintf(stderr, "minuid_bin2str() failed\n");
			return 1;
		}
		printf("%s\n", txt);
	}

	return 0;
}

int main(int argc, char *argv[])
{
	char *cmd = argv[1];
	if (cmd == NULL) {
		fprintf(stderr, "need an argument; try --help\n");
		return 1;
	}

	while(*cmd == '-') cmd++;
	switch(*cmd) {
		case 'h': return help(argv[0]);
		case 'g': return gen();
	}

	fprintf(stderr, "Unknown argument: %s; try --help\n", argv[1]);
	return 1;
}
